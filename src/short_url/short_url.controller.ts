import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Redirect,
  Req,
} from '@nestjs/common';
import { CreateShortUrlDTO } from 'src/dtos/create-short-url.dto';
import { ShortUrlService } from './short_url.service';

@Controller('api/short_urls')
export class ShortUrlController {
  constructor(private readonly shortUrlService: ShortUrlService) {}

  @Get('/')
  index() {
    return this.shortUrlService.list();
  }

  @Post('/')
  @HttpCode(HttpStatus.CREATED)
  create(@Body() createShortUrlDTO: CreateShortUrlDTO) {
    return this.shortUrlService.create(createShortUrlDTO);
  }

  @Get('/stats')
  stats() {
    return this.shortUrlService.stats();
  }

  @Get('/:id')
  show(@Param('id') id: string) {
    return this.shortUrlService.find(parseInt(id));
  }

  @Delete('/:id')
  @HttpCode(HttpStatus.NO_CONTENT)
  destroy(@Param('id') id: string) {
    return this.shortUrlService.destroy(parseInt(id));
  }

  @Get('/:uniqueId/visit')
  @Redirect()
  async visit(
    @Param('uniqueId') uniqueId: string,
    @Req()
    req: {
      connection: { remoteAddress: string };
      headers: { 'user-agent': string };
    },
  ) {
    const url: string = await this.shortUrlService.addVisit(
      uniqueId,
      req.connection.remoteAddress,
      req.headers['user-agent'],
    );

    return { url, statusCode: HttpStatus.FOUND };
  }
}
