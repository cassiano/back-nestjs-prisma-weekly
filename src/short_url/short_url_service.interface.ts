export type ShortUrlDetails = {
  id: number;
  uniqueId: string;
  url: string;
  visited: number;
  links: {
    visit: string;
  };
  createdAt: Date;
  updatedAt: Date;
};

export type ShortUrlStats = {
  shortUrls: {
    generated: number;
    visited: number;
  };
};
