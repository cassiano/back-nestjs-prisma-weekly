import { Injectable } from '@nestjs/common';
import { ShortUrl } from '@prisma/client';
import { CreateShortUrlDTO } from 'src/dtos/create-short-url.dto';
import { PrismaService } from '../prisma.service';
import { ShortUrlDetails, ShortUrlStats } from './short_url_service.interface';

export type ShortUrlWithOptionalVisitCount = ShortUrl & {
  _count?: { visits: number };
};

global.crypto = require('crypto');

@Injectable()
export class ShortUrlService {
  constructor(private prisma: PrismaService) {}

  async list(): Promise<ShortUrlDetails[]> {
    return (
      await this.prisma.shortUrl.findMany({
        orderBy: { updatedAt: 'desc' },
        include: {
          _count: {
            select: { visits: true },
          },
        },
      })
    ).map(this.toJSON);
  }

  async create(shortUrlData: CreateShortUrlDTO): Promise<ShortUrlDetails> {
    return this.toJSON(
      await this.prisma.shortUrl.create({
        data: {
          ...shortUrlData,
          uniqueId: crypto.randomUUID(),
        },
      }),
    );
  }

  async stats(): Promise<ShortUrlStats> {
    const generated = await this.prisma.shortUrl.count();
    const visited = await this.prisma.shortUrlVisit.count();

    return {
      shortUrls: { generated, visited },
    };
  }

  async destroy(id: number): Promise<ShortUrlDetails> {
    return this.toJSON(await this.prisma.shortUrl.delete({ where: { id } }));
  }

  async find(id: number): Promise<ShortUrlDetails> {
    return this.toJSON(await this.findOneBy({ id }));
  }

  async addVisit(
    uniqueId: string,
    ip: string,
    userAgent: string,
  ): Promise<string> {
    const shortUrl = await this.findOneBy({ uniqueId });

    await this.prisma.shortUrlVisit.create({
      data: {
        shortUrlId: shortUrl.id,
        userAgent,
        ip,
        updatedAt: new Date(),
      },
    });

    return shortUrl.url;
  }

  private findOneBy(
    attrs: Partial<ShortUrl>,
  ): Promise<ShortUrlWithOptionalVisitCount> {
    return this.prisma.shortUrl.findFirstOrThrow({
      where: { ...attrs },
      include: {
        _count: {
          select: { visits: true },
        },
      },
    });
    // .catch(() => {
    //   throw new NotFoundException(
    //     `ShortUrl with attributes {${Object.entries(attrs)
    //       .map(([k, v]) => `${k}=${v}`)
    //       .join()}} not found`,
    //   );
    // });
  }

  private toJSON(shortUrl: ShortUrlWithOptionalVisitCount): ShortUrlDetails {
    return {
      ...shortUrl,
      visited: shortUrl._count?.visits ?? 0,
      links: {
        visit: `http://localhost:3001/api/short_urls/${shortUrl.uniqueId}/visit`,
      },
    };
  }
}
